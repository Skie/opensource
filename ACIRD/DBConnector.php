<?php
require_once "Autoloader.php";
/**
 * Created by PhpStorm.
 * User: Joe
 * Date: 10/4/2017
 * Time: 9:05 AM
 */
class DBConnector
{
    public $db;
    private $DBServer = "localhost";
    private $DBUser = "root";
    private $DBpassword ="root";
    private $DBName = "test";

    function dbConn()
    {
        //$conn = new mysqli("localhost", "root", "root", "test");
        $conn = new mysqli($this->DBServer, $this->DBUser, $this->DBpassword, $this->DBName);
        if ($conn->connect_error) {
            echo "Failed";
        }
        return $conn;
    }
}