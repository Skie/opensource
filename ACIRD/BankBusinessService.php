<?php
require_once "AutoLoader.php";
class BankBusinessService
{

    function getCheckingBallance()
    {
        $db = new DBConnector();
        $conn = $db->dbConn();
//Check Checking
        $CDS = new CheckingDataService($conn);
        $balance = $CDS->getBalance();


        //Saving Update
        //$CDS->updateBalance($balance + 100);
        $balance = $CDS->getBalance();

        $conn->close();
        return $balance;
    }

    function getSavingBallance()
    {
        $db = new DBConnector();
        $conn = $db->dbConn();

//Check Saving
        $SDS = new SavingDataService($conn);
        $balance = $SDS->getBalance();


//Update Saving
        //$SDS->updateBalance($balance + 100);
        $balance = $SDS->getBalance();

        $conn->close();
        return $balance;
    }


    function transaction()
    {
        //Get database connection
        $db = new DBConnector();
        $conn = $db->dbConn();

        //Turn Autocommit off.
        $conn->autocommit(false);
        $conn->begin_transaction();

        //Get the balance of the checkings account, then withdraw 100 from it.
        $CDS = new CheckingDataService($conn);
        $BallanceCheck = $CDS->getBalance();
        $UpdateCheck = $CDS->updateBalance($BallanceCheck - 100);

        //Get the balance of the savings account, then deposit 100 to it from the checkings.
        $SDS = new SavingDataService($conn);
        $BallanceSave = $SDS->getBalance();
        $UpdateSave = $SDS->updateBalance($BallanceSave + 100);

        if($UpdateCheck && $UpdateSave)
        {
            $conn->commit();
        }
        else
        {
            $conn->rollback();
        }
        $conn->close();
    }
}