<?php
require_once "AutoLoader.php";

$BBS = new BankBusinessService();

//Old Balance
echo "Checking balance: " . $BBS->getCheckingBallance();
echo "<br>";

echo "Savings balance: " . $BBS->getSavingBallance();

echo "<br>";
echo "<br>";

$BBS->transaction();

//New Balance
echo "New Balance is: " . $BBS->getCheckingBallance();
echo "<br>";
echo "New Balance is: " . $BBS->getSavingBallance();