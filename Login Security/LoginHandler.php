<?php

include ('Header.php');
include "SecurityService.php";

//include_once 'Autoloader.php';


$username = $_POST["username"];
$password = $_POST["password"];
if($username === NULL ||trim($username) ==="")
{

    $message = "User name is a required field" . "<br>";
    echo $message;
}

if($password === NULL ||trim($password) ==="")
{
    $message = "Password is a required field" . "<br>";
    echo $message;
    //echo "password is a required field." . "<br>";
}
else {
    $service = new SecurityService($username, $password);
    $authenticated = ($service->authenticate());
    if ($authenticated == true) {
        //echo "test5";
        $_SESSION["principle"] = true;
        include "LoginPassed.php";

    } else {
        //echo "test6";
        $_SESSION["principle"] = false;
        include "loginFailed.php";
    }
}