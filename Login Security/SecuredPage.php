<?php
include "header.php";

if(isset($_SESSION["principle"]) == false || $_SESSION["principle"] == null || $_SESSION["principle"] == false)
{
    header(string: "Location: Login.html");
}